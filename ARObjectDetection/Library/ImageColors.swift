//  Based On
//  UIImageColors.swift
//  https://github.com/jathu/UIImageColors
//

//  Created by Lukas Kugler 2019-03-06 - Stuttgart

import UIKit

public struct UIImageColors {
    public var background: UIColor!
    public var primary: UIColor!
    public var secondary: UIColor!
    public var detail: UIColor!
    
    public init(background: UIColor, primary: UIColor, secondary: UIColor, detail: UIColor) {
        self.background = background
        self.primary = primary
        self.secondary = secondary
        self.detail = detail
    }
}

public enum UIImageColorsQuality: CGFloat {
    case lowest = 50 // 50px
    case low = 100 // 100px
    case high = 250 // 250px
    case highest = 0 // No scale
}

fileprivate struct UIImageColorsCounter {
    let color: Double
    let count: Int
    init(color: Double, count: Int) {
        self.color = color
        self.count = count
    }
}

fileprivate struct UIImageColorSegment {
    let name: String
    var count: Int
    init(name: String) {
        self.name = name
        self.count = 0
    }
}

/*
 Extension on double that replicates UIColor methods. We DO NOT want these
 exposed outside of the library because they don't make sense outside of the
 context of UIImageColors.
 */
fileprivate extension Double {
    
    private var r: Double {
        return fmod(floor(self/1000000),1000000)
    }
    
    private var g: Double {
        return fmod(floor(self/1000),1000)
    }
    
    private var b: Double {
        return fmod(self,1000)
    }
    
    fileprivate var isDarkColor: Bool {
        return (r*0.2126) + (g*0.7152) + (b*0.0722) < 127.5
    }
    
    fileprivate var isBlackOrWhite: Bool {
        return (r > 232 && g > 232 && b > 232) || (r < 23 && g < 23 && b < 23)
        //return false
    }
    
    fileprivate func isDistinct(_ other: Double) -> Bool {
        let _r = self.r
        let _g = self.g
        let _b = self.b
        let o_r = other.r
        let o_g = other.g
        let o_b = other.b
        
        return (fabs(_r-o_r) > 63.75 || fabs(_g-o_g) > 63.75 || fabs(_b-o_b) > 63.75)
            && !(fabs(_r-_g) < 7.65 && fabs(_r-_b) < 7.65 && fabs(o_r-o_g) < 7.65 && fabs(o_r-o_b) < 7.65)
    }
    
    
    // Compares two colors depending on their Hue and Saturation (not Value to negate the differences of lightning conditions).
    // If the two colors are different, this function returns true. If they roughly similar colors, it returns false.
    fileprivate func compareHSV(_ other: Double) -> Bool {
        // Convert RGB to HSV
        /*
        let _r = self.r
        let _g = self.g
        let _b = self.b
        let o_r = other.r
        let o_g = other.g
        let o_b = other.b
        
        var _H, _S, _V: Double
        let _M = fmax(_r,fmax(_g, _b))
        let _C = _M-fmin(_r,fmin(_g, _b))
        
        _V = _M
        _S = _V == 0 ? 0:_C/_V
        
        if _C == 0 {
            _H = 0
        } else if _r == _M {
            _H = fmod((_g-_b)/_C, 6)
        } else if _g == _M {
            _H = 2+((_b-_r)/_C)
        } else {
            _H = 4+((_r-_g)/_C)
        }
        
        if _H < 0 {
            _H += 6
        }
        
        
        var o_H, o_S, o_V: Double
        let o_M = fmax(o_r,fmax(o_g, o_b))
        let o_C = o_M-fmin(o_r,fmin(o_g, o_b))
        
        o_V = o_M
        o_S = o_V == 0 ? 0:o_C/o_V
        
        if o_C == 0 {
            o_H = 0
        } else if o_r == o_M {
            o_H = fmod((o_g-o_b)/o_C, 6)
        } else if o_g == o_M {
            o_H = 2+((o_b-o_r)/o_C)
        } else {
            o_H = 4+((o_r-o_g)/o_C)
        }
        
        if o_H < 0 {
            o_H += 6
        }
        */
        
        let colorHSV = self.convertToHSV()
        let otherHSV = other.convertToHSV()
        
        let _H = colorHSV[0]
        let _S = colorHSV[1]
        let _V = colorHSV[2]
        
        let o_H = otherHSV[0]
        let o_S = otherHSV[1]
        let o_V = otherHSV[2]
        
        //print("\(_H/360) - \(o_H/360) = \(abs(_H/360 - o_H/360)), \(_S) - \(o_S) = \(abs(_S - o_S))")
        
        if ((abs(_H/360 - o_H/360) > 0.4) && (abs(_S - o_S) > 0.2)) || (abs(_H/360 - o_H/3607) > 0.5) || (abs(_S - o_S) > 0.5) {
            //print("\(_H/360) - \(o_H/360) = \(abs(_H/360 - o_H/360)), \(_S) - \(o_S) = \(abs(_S - o_S))")
            //print("true!")
        }
        return ((abs(_H/360 - o_H/360) > 0.4) && (abs(_S - o_S) > 0.2)) || (abs(_H/360 - o_H/360) > 0.5) || (abs(_S - o_S) > 0.5)
        
    }
    
    fileprivate func convertToHSV() -> [Double] {
        let r = self.r
        let g = self.g
        let b = self.b
        
        
        let min = r < g ? (r < b ? r : b) : (g < b ? g : b)
        let max = r > g ? (r > b ? r : b) : (g > b ? g : b)
        
        let v = max
        let delta = max - min
        
        guard delta > 0.00001 else { return [0, 0, max] }
        guard max > 0 else { return [-1, 0, v] } // Undefined, achromatic grey
        let s = delta / max
        
        let hue: (Double, Double) -> Double = { max, delta -> Double in
            if r == max { return (g-b)/delta } // between yellow & magenta
            else if g == max { return 2 + (b-r)/delta } // between cyan & yellow
            else { return 4 + (r-g)/delta } // between magenta & cyan
        }
        
        let h = hue(max, delta) * 60 // In degrees
        return [(h < 0 ? h+360 : h), s, v/255]
    }
    
    fileprivate func with(minSaturation: Double) -> Double {
        // Ref: https://en.wikipedia.org/wiki/HSL_and_HSV
        
        // Convert RGB to HSV
        
        let _r = r/255
        let _g = g/255
        let _b = b/255
        var H, S, V: Double
        let M = fmax(_r,fmax(_g, _b))
        var C = M-fmin(_r,fmin(_g, _b))
        
        V = M
        S = V == 0 ? 0:C/V
        
        if minSaturation <= S {
            return self
        }
        
        if C == 0 {
            H = 0
        } else if _r == M {
            H = fmod((_g-_b)/C, 6)
        } else if _g == M {
            H = 2+((_b-_r)/C)
        } else {
            H = 4+((_r-_g)/C)
        }
        
        if H < 0 {
            H += 6
        }
        
        // Back to RGB
        
        C = V*minSaturation
        let X = C*(1-fabs(fmod(H,2)-1))
        var R, G, B: Double
        
        switch H {
        case 0...1:
            R = C
            G = X
            B = 0
        case 1...2:
            R = X
            G = C
            B = 0
        case 2...3:
            R = 0
            G = C
            B = X
        case 3...4:
            R = 0
            G = X
            B = C
        case 4...5:
            R = X
            G = 0
            B = C
        case 5..<6:
            R = C
            G = 0
            B = X
        default:
            R = 0
            G = 0
            B = 0
        }
        
        let m = V-C
        
        return (floor((R + m)*255)*1000000)+(floor((G + m)*255)*1000)+floor((B + m)*255)
    }
    
    fileprivate func isContrasting(_ color: Double) -> Bool {
        let bgLum = (0.2126*r)+(0.7152*g)+(0.0722*b)+12.75
        let fgLum = (0.2126*color.r)+(0.7152*color.g)+(0.0722*color.b)+12.75
        if bgLum > fgLum {
            return 1.6 < bgLum/fgLum
        } else {
            return 1.6 < fgLum/bgLum
        }
    }
    
    fileprivate var uicolor: UIColor {
        return UIColor(red: CGFloat(r)/255, green: CGFloat(g)/255, blue: CGFloat(b)/255, alpha: 1)
    }
    
    fileprivate var pretty: String {
        return "\(Int(self.r)), \(Int(self.g)), \(Int(self.b))"
    }
}

extension UIImage {
    private func resizeForUIImageColors(newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        defer {
            UIGraphicsEndImageContext()
        }
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else {
            fatalError("UIImageColors.resizeForUIImageColors failed: UIGraphicsGetImageFromCurrentImageContext returned nil.")
        }
        
        return result
    }
    
    
    public func getColors(quality: UIImageColorsQuality = .high, _ completion: @escaping ([String]) -> Void) {
        DispatchQueue.global().async {
            let result = self.getColors(quality: quality)
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    // Get the four dominant colors of a UIImage
    public func getColors(quality: UIImageColorsQuality = .high) -> [String] {
        var scaleDownSize: CGSize = self.size
        if quality != .highest {
            if self.size.width < self.size.height {
                let ratio = self.size.height/self.size.width
                scaleDownSize = CGSize(width: quality.rawValue/ratio, height: quality.rawValue)
            } else {
                let ratio = self.size.width/self.size.height
                scaleDownSize = CGSize(width: quality.rawValue, height: quality.rawValue/ratio)
            }
        }
        
        let cgImage = self.resizeForUIImageColors(newSize: scaleDownSize).cgImage!
        let width: Int = cgImage.width
        let height: Int = cgImage.height
        
        let threshold = Int(CGFloat(height)*0.01)
        var proposed: [Double] = [-1,-1,-1,-1]
        
        guard let data = CFDataGetBytePtr(cgImage.dataProvider!.data) else {
            fatalError("UIImageColors.getColors failed: could not get cgImage data.")
        }
        
        let imageColors = NSCountedSet(capacity: width*height)
        for x in 0..<width {
            for y in 0..<height {
                let pixel: Int = ((width * y) + x) * 4
                if 127 <= data[pixel+3] {
                    imageColors.add((Double(data[pixel+2])*1000000)+(Double(data[pixel+1])*1000)+(Double(data[pixel])))
                }
            }
        }
        
      



        
        
        
        let sortedColorComparator: Comparator = { (main, other) -> ComparisonResult in
            let m = main as! UIImageColorsCounter, o = other as! UIImageColorsCounter
            if m.count < o.count {
                return .orderedDescending
            } else if m.count == o.count {
                return .orderedSame
            } else {
                return .orderedAscending
            }
        }
        
        let sortedColorSegmentComparator: Comparator = { (main, other) -> ComparisonResult in
            let m = main as! UIImageColorSegment, o = other as! UIImageColorSegment
            if m.count < o.count {
                return .orderedDescending
            } else if m.count == o.count {
                return .orderedSame
            } else {
                return .orderedAscending
            }
        }
        
        
        
        
        let enumerator = imageColors.objectEnumerator()
        let sortedColors = NSMutableArray(capacity: imageColors.count)
        while let K = enumerator.nextObject() as? Double {
            let C = imageColors.count(for: K)
            if threshold < C {
                sortedColors.add(UIImageColorsCounter(color: K, count: C))
            }
        }
        sortedColors.sort(comparator: sortedColorComparator)
        
        
        var red = UIImageColorSegment(name: "red")
        var orange = UIImageColorSegment(name: "orange")
        var yellow = UIImageColorSegment(name: "yellow")
        var green = UIImageColorSegment(name: "green")
        var turqoise = UIImageColorSegment(name: "turqoise")
        var lightblue = UIImageColorSegment(name: "lightblue")
        var darkblue = UIImageColorSegment(name: "darkblue")
        var purple = UIImageColorSegment(name: "purple")
        var pink = UIImageColorSegment(name: "pink")
        var black = UIImageColorSegment(name: "black")
        var white = UIImageColorSegment(name: "white")
        var grey = UIImageColorSegment(name: "grey")
        var brown = UIImageColorSegment(name: "brown")
        
        
        for color in sortedColors {
            let color = (color as! UIImageColorsCounter).color
            let hsvColor = color.convertToHSV()
            //print("\(hsvColor)")
            
            //black and white
            if (hsvColor[2] < 0.1) {
                
            //print("black")
                black.count += 1
                continue
            }
            if (hsvColor[1] < 0.25 && hsvColor[2] > 0.75) {
                //print("white")
                white.count += 1
                 continue
            }
            //grey
            
            //print("grey")
            if (hsvColor[1] < 0.2) {
                //grey.count += 1
                continue
            }
            
            
            //check for brown first, since it's the most common by far
            //if (hsvColor[1] < 0.7 && hsvColor[1] > 0.25 && hsvColor[2] > 0.1 && hsvColor[2] < 0.75 && hsvColor[0] > 20 && hsvColor[0] < 40) {
                //print("brown")
                //brown.count += 1
                //continue
            //}
            switch hsvColor[0] {
            case 0..<10:
                red.count += 1
            case 20..<40:
                orange.count += 1
            case 40..<60:
                yellow.count += 1
            case 60..<150:
                green.count += 1
            case 150..<170:
                turqoise.count += 1
            case 170..<190:
                lightblue.count += 1
            case 190..<250:
                darkblue.count += 1
            case 250..<290:
                purple.count += 1
            case 290..<330:
                pink.count += 1
            case 330..<360:
                red.count += 1
            default:
                continue
            }
        }
        
        var allColors: NSMutableArray = [red, orange, yellow, green, turqoise, lightblue, darkblue, purple, pink, black, white, grey, brown]
        allColors.sort(comparator: sortedColorSegmentComparator)
        
        var test = allColors as NSArray as! [UIImageColorSegment]
        
        //print(test)

        
        /*
        proposed[0] = (sortedColors[0] as! UIImageColorsCounter).color
        
        loop: for color in sortedColors {
            let color = (color as! UIImageColorsCounter).color
            
            if proposed[1] == -1 {
                if color.compareHSV(proposed[0]) {
                    proposed[1] = color
                }
            } else if proposed[2] == -1 {
                if color.compareHSV(proposed[0]) && color.compareHSV(proposed[1]) {
                    proposed[2] = color
                }
                proposed[2] = color
            } else if proposed[3] == -1 {
                if color.compareHSV(proposed[0]) && color.compareHSV(proposed[1]) && color.compareHSV(proposed[2]) {
                    proposed[3] = color
                    break loop
                }
            }
        }
        
        //print("Proposing: \(proposed[0].uicolor), \(proposed[1].uicolor), \(proposed[2].uicolor), \(proposed[3].uicolor)")
        */
        
        if test[3].count > 40 {
            return [test[0].name, test[1].name, test[2].name, test[3].name]
        } else if test[2].count > 40 {
            return [test[0].name, test[1].name, test[2].name]
        } else if test[1].count > 40 {
            return [test[0].name, test[1].name]
        } else {
            return [test[0].name]
        }
        
    }
}

