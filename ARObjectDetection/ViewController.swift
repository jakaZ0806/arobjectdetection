import CoreMedia
import CoreML
import UIKit
import Vision
import ARKit
import SceneKit
// import this
import AVFoundation

class ViewController: UIViewController, ARSCNViewDelegate, ARSessionDelegate {

  @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var previewImage: UIImageView!
    @IBOutlet weak var infoTextView: UITextView!
    
    @IBOutlet weak var statusTextView: UITextView!
    
  var currentBuffer: CVPixelBuffer?
  var viewPortTransform = CGAffineTransform()
    
  let bubbleDepth : Float = 0.01 // the 'depth' of 3D text
  
  var objectDetectionActive = false
  var findingWorldOrigin = true
  var planeDetectionActive = false
  var rectangleDetectionActive = false
    
  var searchingForRectangles = false
    var searchingForBarcodes = false
    
  var waitingForDistance = false
    
   var startTime = DispatchTime.now()
    var currentCameraPosition: SCNVector3 = SCNVector3()
    
  var frameCounter: UInt8 = 0
    
    
    // create a sound ID, in this case its the tweet sound.
    let systemSoundID: SystemSoundID = 1016
    
    var packages: [packageData] = []
    
    
    var anchorNodes: [String: SCNNode] = [:]
    var anchors: [String: ARImageAnchor] = [:]
    
    var planes = [UUID: Plane]()
    
    var packageToFind: packageData = packageData(number: 1, length: 0.275, width: 0.24, height: 0.17, top: packageColors(primary: "brown", others: ["orange" , "white"]), front: packageColors(primary: "brown", others: ["white", "orange", "darkblue"]), position: SCNVector3(x: 0.0, y: 0.0 ,z: -0.3), barcode: "")


  let dispatchQueueML = DispatchQueue(label: "com.hw.dispatchqueueml") // A Serial Queue
    /// A serial queue for thread safety when modifying the SceneKit node graph.
  let updateQueue = DispatchQueue(label: Bundle.main.bundleIdentifier! +
        ".serialSceneKitQueue")
    
    let dispatchQueueColor = DispatchQueue(label: "com.hw.dispatchqueuecl")
    

  let coreMLModel = MobileNetV2_SSDLite()

  lazy var visionModel: VNCoreMLModel = {
    do {
      return try VNCoreMLModel(for: coreMLModel.model)
    } catch {
      fatalError("Failed to create VNCoreMLModel: \(error)")
    }
  }()

  lazy var visionRequest: VNCoreMLRequest = {
    let request = VNCoreMLRequest(model: visionModel, completionHandler: {
      [weak self] request, error in
      self?.processObservations(for: request, error: error)
    })

    // NOTE: If you use another crop/scale option, you must also change
    // how the BoundingBoxView objects get scaled when they are drawn.
    // Currently they assume the full input image is used.
    request.imageCropAndScaleOption = .scaleFill
    return request
  }()

  let maxBoundingBoxViews = 10
  var boundingBoxViews = [BoundingBoxView]()
  var colors: [String: UIColor] = [:]

  override func viewDidLoad() {
    super.viewDidLoad()
    setUpBoundingBoxViews()
    
    startTime = DispatchTime.now()
    statusTextView.text = "Looking for World Origin Markers."
    
    
    previewImage.contentMode = .scaleAspectFit

    // Set ARKit Preferences
    sceneView.delegate = self
    sceneView.session.delegate = self
    sceneView.showsStatistics = true
    sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints, ARSCNDebugOptions.showWorldOrigin]
    sceneView.autoenablesDefaultLighting = true
    sceneView.preferredFramesPerSecond = 30
    
    // Tap Gesture Recognizer
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(gestureRecognize:)))
    view.addGestureRecognizer(tapGesture)
    
    // Create the Views for Bounding Boxes
    setupBoundingBoxes()
    
  }
    
    // enables 6DoF Tracking via sensors and camera
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        guard let referenceImages = ARReferenceImage.referenceImages(inGroupNamed: "AR Resources", bundle: nil) else {
            fatalError("Missing expected asset catalog resources.")
        }
        
        /*
        World Tracking
            -> tracks World around device, plane detection, hit test, ... possible
            -> image tracking updates not in real time
         */
        
        let configuration = ARWorldTrackingConfiguration()
        configuration.detectionImages = referenceImages
        configuration.planeDetection = [.horizontal, .vertical]
        
        /*
         Image Tracking
            -> no world tracking possible
            -> image tracking in real time with frequent updates
        */
        //let configuration = ARImageTrackingConfiguration()
        //configuration.trackingImages = referenceImages
        
        // Start AR Session
        sceneView.session.run(configuration, options: .resetTracking)
    }
    
    // disables world tracking
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }

    
    //Creates Views for the Bounding Boxes
  func setUpBoundingBoxViews() {
    for _ in 0..<maxBoundingBoxViews {
      boundingBoxViews.append(BoundingBoxView())
    }

    // The label names are stored inside the MLModel's metadata.
    guard let userDefined = coreMLModel.model.modelDescription.metadata[MLModelMetadataKey.creatorDefinedKey] as? [String: String],
       let allLabels = userDefined["classes"] else {
      fatalError("Missing metadata")
    }

    let labels = allLabels.components(separatedBy: ",")

    // Assign random colors to the classes.
    for label in labels {
      colors[label] = UIColor(red: CGFloat.random(in: 0...1),
                              green: CGFloat.random(in: 0...1),
                              blue: CGFloat.random(in: 0...1),
                              alpha: 1)
    }
    
    
  }

  func setupBoundingBoxes() {
        // Add the bounding box layers to the UI, on top of the video preview.
        for box in self.boundingBoxViews {
          box.addToLayer(self.sceneView.layer)
        }
  }

    
  /**
     Takes an ARFrame and Detects Objects.
 */
  func predict(frame: ARFrame) {
    let imageBuffer = frame.capturedImage
      // Get additional info from the camera.
      var options: [VNImageOption : Any] = [:]
      if let cameraIntrinsicMatrix = CMGetAttachment(imageBuffer, key: kCMSampleBufferAttachmentKey_CameraIntrinsicMatrix, attachmentModeOut: nil) {
        options[.cameraIntrinsics] = cameraIntrinsicMatrix
      }

    let handler = VNImageRequestHandler(cvPixelBuffer: imageBuffer, orientation: CGImagePropertyOrientation.right, options: [:])
      do {
        try handler.perform([self.visionRequest])
      } catch {
        print("Failed to perform Vision request: \(error)")
      }
  }
    
  func processObservations(for request: VNRequest, error: Error?) {
    DispatchQueue.main.async {
      if let results = request.results as? [VNRecognizedObjectObservation] {
        self.show(predictions: results)
      } else {
        self.show(predictions: [])
      }
    }
  }

  func show(predictions: [VNRecognizedObjectObservation]) {
    for i in 0..<boundingBoxViews.count {
      if i < predictions.count {
        let prediction = predictions[i]

        /*
         The predicted bounding box is in normalized image coordinates, with
         the origin in the lower-left corner.
         Scale the bounding box to the coordinate system of the input image which has a aspect ratio of 4:3.
         Then scale to viewport of the AR Sceneview.
        */
        let width = self.sceneView.bounds.width
        let height = width * 4 / 3
        let offsetY = (self.sceneView.bounds.height - height) / 2
        
        //let height = sceneView.bounds.height
        //let width = sceneView.bounds.height * 3 / 4
        //let offsetX = (self.sceneView.bounds.width - width) / 2
        
        
        let scale = CGAffineTransform.identity.scaledBy(x: width, y: height)
        let transform = CGAffineTransform(scaleX: 1, y: -1).translatedBy(x: 0, y: -height - offsetY)
    
        //Somehow, Bounding Boxes are rotated 90 by degrees. This transformation moves the bounding Box to (0, 0) since rotation is always around (0, 0), turns it 90 degrees clockwise and then moves it back.
        let rotation = CGAffineTransform.identity.translatedBy(x: width / 2, y: height / 2).rotated(by: -.pi/2).translatedBy(x: -width / 2, y: -height / 2)

        let rect = prediction.boundingBox.applying(viewPortTransform).applying(scale).applying(rotation).applying(transform)

        //let rect = convertFromCamera(prediction.boundingBox)
        
        // The labels array is a list of VNClassificationObservation objects,
        // with the highest scoring class first in the list.
        let bestClass = prediction.labels[0].identifier
        let confidence = prediction.labels[0].confidence

        // Show the bounding box.
        let label = String(format: "%@ %.1f", bestClass, confidence * 100)
        let color = colors[bestClass] ?? UIColor.red
        //print("\(label) at \(rect.midX), \(rect.midY)")
        boundingBoxViews[i].show(frame: rect, label: label, color: color)
      } else {
        boundingBoxViews[i].hide()
      }
    }
  }
    
    
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        // get the transformation necessary to scale bounding boxes to the current viewPort of the AR scene.
        let orient = UIApplication.shared.statusBarOrientation
        let viewportSize = view.frame.size
        let newTransform = frame.displayTransform(for: orient, viewportSize: viewportSize)
        let currentTransform = frame.camera.transform
        self.currentCameraPosition = SCNVector3Make(currentTransform.columns.3.x, currentTransform.columns.3.y, currentTransform.columns.3.z)
        viewPortTransform = newTransform
        //Perform Object Detection only every few frames to improve overall performance.
        if objectDetectionActive == true && frameCounter % 5 == 0 {
                dispatchQueueML.async {
                    self.predict(frame: frame)
            }
        
        }
        //Look for Rectangles and Barcodes
        if rectangleDetectionActive == true && frameCounter % 5 == 0 {
            dispatchQueueML.async {
                self.findRectangle(frame: frame)
            }
            updateQueue.async {
                self.findBarcode(frame: frame)
            }
            //Start Rectangle Detection when camera is near target location
        } else if (waitingForDistance == true) {
            let distance = SCNVector3.distanceFrom(vector: self.currentCameraPosition, toVector: packageToFind.getPosition())
            //print("Distance \(distance)")
            if (distance < 1.5) {
                rectangleDetectionActive = true
                waitingForDistance = false
            }
        }
        frameCounter &+= 1  //This operator allows Integer Overflow. So after 255, frameCounter becomes 0 again.
        
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        if let imageAnchor = anchor as? ARImageAnchor {
            print("Card detected")
            if findingWorldOrigin == false {return}
            let referenceImage = imageAnchor.referenceImage

            updateQueue.async {
                
                // Create a plane to visualize the initial position of the detected image.
                let plane = SCNPlane(width: referenceImage.physicalSize.width,
                                     height: referenceImage.physicalSize.height)
                let planeNode = SCNNode(geometry: plane)
                planeNode.opacity = 0.25
                /*
                let distance = SCNVector3.distanceFrom(vector: self.currentCameraPosition, toVector: node.position)
                self.infoTextView.text.append("Found Marker d: \(distance) \n")
                */
                /*
                 `SCNPlane` is vertically oriented in its local coordinate space, but
                 `ARImageAnchor` assumes the image is horizontal in its local space, so
                 rotate the plane to match.
                 */
                planeNode.eulerAngles.x = -.pi / 2
                
                /*
                 Image anchors are not tracked after initial detection, so create an
                 animation that limits the duration for which the plane visualization appears.
                 */
                planeNode.runAction(self.imageHighlightAction)
                
                // Add the plane visualization to the scene.
                node.addChildNode(planeNode)
                

                self.anchorNodes[referenceImage.name ?? "Card"] = node
                self.anchors[referenceImage.name ?? "Card"] = imageAnchor
                
                if self.anchors.count == 2 {
                    self.findingWorldOrigin = false
                    self.findWorldOrigin()
                    self.findingWorldOrigin = true
                }
                
            }
        } else if let planeAnchor = anchor as? ARPlaneAnchor {
            if planeDetectionActive == false {return}
            print("Found plane: \(planeAnchor)")
            let plane = Plane(anchor: planeAnchor)
            
            // 2
            planes[anchor.identifier] = plane
            
            // 3
            node.addChildNode(plane)
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        
        // 1
        if let plane = planes[planeAnchor.identifier] {
            //2
            plane.updateWith(anchor: planeAnchor)
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        planes.removeValue(forKey: anchor.identifier)
    }
    
    //Look for two world markers to set world origin relative to cargo space
    func findWorldOrigin() {
            //Compare orientation
            if abs((anchorNodes["marker6"]?.eulerAngles.y ?? 0) - (anchorNodes["marker6"]?.eulerAngles.y ?? 999)) < 0.02 {
                var middlePoint = self.posBetween(first: anchorNodes["marker6"]?.position ?? SCNVector3(0,0,0), second: anchorNodes["marker8"]?.position ?? SCNVector3(0,0,0))
                let middleNode = SCNNode()
                //Subtract half of the image size to get point at bottom
                middlePoint.z = middlePoint.z + 0.105
                middleNode.position = middlePoint
                middleNode.eulerAngles.y = (anchorNodes["marker6"]!.eulerAngles.y + anchorNodes["marker8"]!.eulerAngles.y) / 2
                self.sceneView.session.setWorldOrigin(relativeTransform: middleNode.simdTransform)
                self.sceneView.session.setWorldOrigin(relativeTransform: middleNode.simdTransform)
                AudioServicesPlaySystemSound (self.systemSoundID)
                DispatchQueue.main.async {
                    let time = DispatchTime.now()
                    let nanoTime = time.uptimeNanoseconds - self.startTime.uptimeNanoseconds // <<<<< Difference in nano seconds (UInt64)
                    let timeInterval = Double(nanoTime) / 1_000_000_000
                    self.infoTextView.text.append("Found World origin after: \(timeInterval) \n")
                    self.statusTextView.text = "Looking for package"
                }
                self.loadScenario()
            } else {
                //Orientation too different. Reset all anchors and try again
                self.anchorNodes = [:]
                for anchor in self.anchors {
                    self.sceneView.session.remove(anchor: anchor.value)
                }
                self.anchors = [:]
                print("Removed All Anchors!")
            }
        
            //let bubbleNode = createNewBubbleParentNode("middle")
            //middleNode.addChildNode(bubbleNode)
            
        
    }
    
    func loadScenario() {
        self.packages.append(packageData(number: 1, length: 0.25, width: 0.18, height: 0.11, top: packageColors(primary: "white", others: ["orange"]), front: packageColors(primary: "orange", others: ["white"]), position: SCNVector3(x: -0.60, y: 0.0 ,z: -1.32), barcode: "5565436444"))
        
        self.packages.append(packageData(number: 2, length: 0.32, width: 0.2, height: 0.04, top: packageColors(primary: "white", others: ["orange", "red", "black"]), front: packageColors(primary: "white", others:[]), position: SCNVector3(x: 0.32, y: 0.0 ,z: -0.34), barcode: "5466544222"))
        self.packages.append(packageData(number: 3, length: 0.165, width: 0.12, height: 0.11, top: packageColors(primary: "white", others: ["orange", "yellow", "red"]), front: packageColors(primary: "red", others:["orange", "white"] ), position: SCNVector3(x: 0.32, y: 0.0 ,z: -0.60), barcode: "2453663546"))
        self.packages.append(packageData(number: 4, length: 0.37, width: 0.12, height: 0.12, top: packageColors(primary: "white", others: ["orange", "darkblue", "lightblue"]), front: packageColors(primary: "orange", others:[ "white"] ), position: SCNVector3(x: 0.53, y: 0.0 ,z: -0.52), barcode: "73257994121005"))
        self.packages.append(packageData(number: 5, length: 0.38, width: 0.29, height: 0.13, top: packageColors(primary: "yellow", others: ["white", "orange"]), front: packageColors(primary: "yellow", others:["orange"] ), position: SCNVector3(x: 0.40, y: 0.0 ,z: -0.85), barcode: "887161632221"))
        self.packages.append(packageData(number: 6, length: 0.135, width: 0.15, height: 0.3, top: packageColors(primary: "orange", others: ["yellow", "white"]), front: packageColors(primary: "orange", others:[] ), position: SCNVector3(x: 0.64, y: 0.0 ,z: -0.85), barcode: "2655321453"))
        self.packages.append(packageData(number: 7, length: 0.31, width: 0.23, height: 0.15, top: packageColors(primary: "orange", others: ["white"]), front: packageColors(primary: "orange", others:[] ), position: SCNVector3(x: 0.37, y: 0.0 ,z: -1.08), barcode: "73240994127008"))
        self.packages.append(packageData(number: 8, length: 0.7, width: 0.14, height: 0.48, top: packageColors(primary: "orange", others: ["white", "yellow"]), front: packageColors(primary: "orange", others:["white", "red"] ), position: SCNVector3(x: 0.65, y: 0.0 ,z: -1.21), barcode: "2546356435"))
        self.packages.append(packageData(number: 9, length: 0.305, width: 0.24, height: 0.105, top: packageColors(primary: "orange", others: ["white", "purple", "red"]), front: packageColors(primary: "violet", others:["orange", "white"] ), position: SCNVector3(x: 0.45, y: 0.0 ,z: -1.39), barcode: "357077372039"))
    
        self.packages.append(packageData(number: 10, length: 0.3, width: 0.235, height: 0.11, top: packageColors(primary: "orange", others: ["white"]), front: packageColors(primary: "orange", others:[ "black"] ), position: SCNVector3(x: -0.65, y: 0.0 ,z: -1.06), barcode: "4574536543"))
        self.packages.append(packageData(number: 11, length: 0.23, width: 0.16, height: 0.11, top: packageColors(primary: "orange", others: ["yellow", "white"]), front: packageColors(primary: "orange", others:[ "black"] ), position: SCNVector3(x: -0.4, y: 0.0 ,z: -1.2), barcode: "6574565444"))
        self.packages.append(packageData(number: 12, length: 0.30, width: 0.245, height: 0.11, top: packageColors(primary: "orange", others: ["white", "black"]), front: packageColors(primary: "orange", others:[ "black"] ), position: SCNVector3(x: -0.4, y: 0.0 ,z: -1.0), barcode: "6543453654"))
        self.packages.append(packageData(number: 13, length: 0.305, width: 0.235, height: 0.11, top: packageColors(primary: "orange", others: ["white"]), front: packageColors(primary: "orange", others:["black"] ), position: SCNVector3(x: -0.4, y: 0.0 ,z: -0.73), barcode: "1231232346"))
        self.packages.append(packageData(number: 14, length: 0.305, width: 0.23, height: 0.10, top: packageColors(primary: "darkblue", others: ["white", "orange", "black"]), front: packageColors(primary: "black", others:[] ), position: SCNVector3(x: -0.4, y: 0.0 ,z: -0.5), barcode: "1234321421"))
        self.packages.append(packageData(number: 15, length: 0.32, width: 0.20, height: 0.04, top: packageColors(primary: "black", others: ["lightblue", "white"]), front: packageColors(primary: "black", others:[] ), position: SCNVector3(x: -0.40, y: 0.0 ,z: -0.3), barcode: "1234567890"))
    
        self.packages.append(packageData(number: 16, length: 0.34, width: 0.255, height: 0.145, top: packageColors(primary: "orange", others: ["white"]), front: packageColors(primary: "orange", others:[] ), position: SCNVector3(x: 0.45, y: 0.0 ,z: -1.70), barcode: "77067242969306"))
        
        self.packages.append(packageData(number: 17, length: 0.23, width: 0.11, height: 0.11, top: packageColors(primary: "orange", others: ["white"]), front: packageColors(primary: "orange", others:[] ), position: SCNVector3(x: -0.65, y: 0.0 ,z: -0.77), barcode: "3425234554"))
        self.packages.append(packageData(number: 18, length: 0.4, width: 0.31, height: 0.11, top: packageColors(primary: "orange", others: ["white", "lightblue", "darkblue"]), front: packageColors(primary: "orange", others:[] ), position: SCNVector3(x: -0.4, y: 0.0 ,z: -1.5), barcode: "72644069029009"))
        self.packages.append(packageData(number: 19, length: 0.27, width: 0.21, height: 0.14, top: packageColors(primary: "orange", others: ["white"]), front: packageColors(primary: "orange", others:[] ), position: SCNVector3(x: -0.66, y: 0.0 ,z: -1.62), barcode: "334088916973"))
        self.packages.append(packageData(number: 20, length: 0.40, width: 0.305, height: 0.10, top: packageColors(primary: "orange", others: ["white"]), front: packageColors(primary: "orange", others:[] ), position: SCNVector3(x: 0.59, y: 0.0 ,z: -0.34), barcode: "700085324172"))
        
        
        packageToFind = self.packages.randomElement()!
        //packageToFind = self.packages[7]
        print("Package Number \(packageToFind.number) selected.")
        
        self.waitingForDistance = true

        let bubbleNode = createNewBubbleParentNode("Package To Find")
        bubbleNode.position = packageToFind.position
        bubbleNode.position.z += packageToFind.height
        self.sceneView.scene.rootNode.addChildNode(bubbleNode)
    }
    
    //Calculate middle point between two points in world coordinate space
    func posBetween(first: SCNVector3, second: SCNVector3) -> SCNVector3 {
        return SCNVector3Make((first.x + second.x) / 2, (first.y + second.y) / 2, (first.z + second.z) / 2)
    }
    
    /*
     Performs a hit test against the real world at the center of all detected objects in the current frame.
     Creates a label at the nearest feature point to the hit test result in the AR environment.
     */
    @objc func handleTap(gestureRecognize: UITapGestureRecognizer) {
        /*
        // Disable detection until labels are created. This ensures that the bounding Boxes are currently rendered.
        objectDetectionActive = false
        for object in boundingBoxViews {
            if object.shapeLayer.isHidden == false {
                let arHitTestResults : [ARHitTestResult] = sceneView.hitTest(CGPoint(x: object.shapeLayer.path!.boundingBox.midX, y: object.shapeLayer.path!.boundingBox.midY), types: [.featurePoint])
                                
                if let closestResult = arHitTestResults.first {
                    // Get Coordinates of HitTest
                    let transform : matrix_float4x4 = closestResult.worldTransform
                    let worldCoord : SCNVector3 = SCNVector3Make(transform.columns.3.x, transform.columns.3.y, transform.columns.3.z)
                    
                    //print("Hit Test at \(object.shapeLayer.path!.boundingBox.midX), \(object.shapeLayer.path!.boundingBox.midY)")
                    //print("Creating Node \(object.textLayer.string as! String) at \(worldCoord.x), \(worldCoord.y), \(worldCoord.z), ")
                    
                    // Create 3D Text
                    let node : SCNNode = createNewBubbleParentNode(object.textLayer.string as! String)
                    sceneView.scene.rootNode.addChildNode(node)
                    node.position = worldCoord
                }
            }
        }
        objectDetectionActive = true
        */
        let time = DispatchTime.now()
        let nanoTime = time.uptimeNanoseconds - self.startTime.uptimeNanoseconds // <<<<< Difference in nano seconds (UInt64)
        let timeInterval = Double(nanoTime) / 1_000_000_000
        self.infoTextView.text.append("Tap: \(timeInterval) \n")
        
    }
    //Scans for barcodes in the current frame and checks whether the barcode matches the barcode of the package to find
    private func findBarcode(frame currentFrame: ARFrame) {
        //print("hallo")
        searchingForBarcodes = true
        
        DispatchQueue.global(qos: .background).async {
            let request = VNDetectBarcodesRequest(completionHandler: {(request, error) in
                DispatchQueue.main.async {
                    self.searchingForBarcodes = false
                    
                    guard let observations = request.results as? [VNBarcodeObservation],
                        let _ = observations.first else {
                            return
                    }
                    
                    for observation in observations {
                        print(observation.payloadStringValue)
                    if (observation.payloadStringValue == self.packageToFind.barcodeValue) {
                        //let center = self.convertFromCamera(CGPoint(x: observation.boundingBox.midX, y: observation.boundingBox.midY)).applying(self.viewPortTransform)
                        
                        let center = self.convertFromCamera(observation.bottomLeft).midBetween(self.convertFromCamera(observation.topRight) )
                        var centerWorldCoord : SCNVector3 = SCNVector3()
                        let centerHitTest: [ARHitTestResult] = self.sceneView.hitTest(center, types: [.featurePoint])
                        if let centerCoord = centerHitTest.first {
                            // Get Coordinates of HitTest
                            let transform : matrix_float4x4 = centerCoord.worldTransform
                            centerWorldCoord = SCNVector3Make(transform.columns.3.x, transform.columns.3.y, transform.columns.3.z)
                        }
                        if (SCNVector3.distanceFrom(vector:
                            centerWorldCoord, toVector: self.packageToFind.position) < 0.4) {
                            self.rectangleDetectionActive = false
                            let distance = SCNVector3.distanceFrom(vector: self.currentCameraPosition, toVector: self.packageToFind.getPosition())
                            print("Package \(self.packageToFind.number) found! Barcode. Distance: \(distance)")
                            DispatchQueue.main.async {
                                let bubbleNode = self.createNewBubbleParentNode("HitTestLocation")
                                bubbleNode.position = centerWorldCoord
                                self.sceneView.scene.rootNode.addChildNode(bubbleNode)
                                //self.previewImage.image = UIImage(ciImage: deskewedImage)
                                // to play sound
                                AudioServicesPlaySystemSound (self.systemSoundID)
                                let time = DispatchTime.now()
                                let nanoTime = time.uptimeNanoseconds - self.startTime.uptimeNanoseconds // <<<<< Difference in nano seconds (UInt64)
                                let timeInterval = Double(nanoTime) / 1_000_000_000
                                self.infoTextView.text.append("Number \(self.packageToFind.number), time: \(timeInterval), distance: \(distance)\n")
                                self.statusTextView.text = "Package \(self.packageToFind.number) found! Barcode."
                            }
                            break
                            
                        }
                    }
                        
                        
                        
                    }
                }
            })
            
            request.symbologies = [VNBarcodeSymbology .I2of5, VNBarcodeSymbology .I2of5Checksum]
            
            // Perform request
            let handler = VNImageRequestHandler(cvPixelBuffer: currentFrame.capturedImage, options: [:])
            try? handler.perform([request])
        }
    }
    
    //Looks for package in current frame using size and color
    private func findRectangle(frame currentFrame: ARFrame) {
        // Note that we're actively searching for rectangles
        searchingForRectangles = true
        //selectedRectangleObservation = nil
        
        // Perform request on background thread
        DispatchQueue.global(qos: .background).async {
            let request = VNDetectRectanglesRequest(completionHandler: { (request, error) in
                
                // Jump back onto the main thread
                DispatchQueue.main.async {
                    
                    // Mark that we've finished searching for rectangles
                    self.searchingForRectangles = false
                    
                    // Access the first result in the array after casting the array as a VNClassificationObservation array
                    guard let observations = request.results as? [VNRectangleObservation],
                        let _ = observations.first else {
                            //print ("No results")
                            return
                    }
                    
                    //print("\(observations.count) rectangles found")

                    // Outline selected rectangle
                    for i in 0..<self.boundingBoxViews.count {
                        if i < observations.count {
                            let prediction = observations[i]
                            let points = [prediction.topLeft, prediction.topRight, prediction.bottomRight, prediction.bottomLeft]
                            let center = self.convertFromCamera(CGPoint(x: prediction.boundingBox.midX, y: prediction.boundingBox.midY))
                            let convertedPoints = points.map { self.convertFromCamera($0) }
                            for point in convertedPoints {
                                point.applying(self.viewPortTransform)
                            }
                            
                            //Hit test every corner of rectangle
                            var bottomRightWorldCoord : SCNVector3 = SCNVector3()
                            var bottomLeftWorldCoord : SCNVector3 = SCNVector3()
                            var topRightWorldCoord : SCNVector3 = SCNVector3()
                            var topLeftWorldCoord : SCNVector3 = SCNVector3()
                            var centerWorldCoord : SCNVector3 = SCNVector3()
                            
                            let topLeftHitTest : [ARHitTestResult] = self.sceneView.hitTest(convertedPoints[0], types: [.featurePoint])
                            let topRightHitTest : [ARHitTestResult] = self.sceneView.hitTest(convertedPoints[1], types: [.featurePoint])
                            let bottomRightHitTest : [ARHitTestResult] = self.sceneView.hitTest(convertedPoints[2], types: [.featurePoint])
                            let bottomLeftHitTest : [ARHitTestResult] = self.sceneView.hitTest(convertedPoints[3], types: [.featurePoint])
                            let centerHitTest: [ARHitTestResult] = self.sceneView.hitTest(center, types: [.featurePoint])
                            
                            if let bottomRightCoord = bottomRightHitTest.first {
                                // Get Coordinates of HitTest
                                let transform : matrix_float4x4 = bottomRightCoord.worldTransform
                                bottomRightWorldCoord = SCNVector3Make(transform.columns.3.x, transform.columns.3.y, transform.columns.3.z)
                                //print("BR")
                            }
                            if let bottomLeftCoord = bottomLeftHitTest.first {
                                // Get Coordinates of HitTest
                                let transform : matrix_float4x4 = bottomLeftCoord.worldTransform
                                bottomLeftWorldCoord = SCNVector3Make(transform.columns.3.x, transform.columns.3.y, transform.columns.3.z)
                                //print("BL")
                            }
                            if let topRightCoord = topRightHitTest.first {
                                // Get Coordinates of HitTest
                                let transform : matrix_float4x4 = topRightCoord.worldTransform
                                topRightWorldCoord = SCNVector3Make(transform.columns.3.x, transform.columns.3.y, transform.columns.3.z)
                                //print("TR")

                            }
                            if let topLeftCoord = topLeftHitTest.first {
                                // Get Coordinates of HitTest
                                let transform : matrix_float4x4 = topLeftCoord.worldTransform
                                topLeftWorldCoord = SCNVector3Make(transform.columns.3.x, transform.columns.3.y, transform.columns.3.z)
                               //print("TL")
                            }
                            if let centerCoord = centerHitTest.first {
                                // Get Coordinates of HitTest
                                let transform : matrix_float4x4 = centerCoord.worldTransform
                                centerWorldCoord = SCNVector3Make(transform.columns.3.x, transform.columns.3.y, transform.columns.3.z)
                                //print("TL")
                            }
                            
                            let lengthR = SCNVector3.distanceFrom(vector: topRightWorldCoord, toVector: bottomRightWorldCoord)
                            let lengthL = SCNVector3.distanceFrom(vector: topLeftWorldCoord, toVector: bottomLeftWorldCoord)
                            let lengthT = SCNVector3.distanceFrom(vector: topRightWorldCoord, toVector: topLeftWorldCoord)
                            let lengthB = SCNVector3.distanceFrom(vector: bottomRightWorldCoord, toVector: bottomLeftWorldCoord)
                            
                            let topVector = topRightWorldCoord - topLeftWorldCoord
                            let leftVector = topLeftWorldCoord - bottomLeftWorldCoord
                            let bottomVector = bottomRightWorldCoord - bottomLeftWorldCoord
                            let rightVector = topRightWorldCoord - bottomRightWorldCoord
                            //print("Measure: \(lengthR)x\(lengthT) // \(lengthL)x\(lengthB)")
                            
                            //Compare real-world length of both sides of the rectangle
                            let middlepoint = self.posBetween(first: topRightWorldCoord, second: bottomLeftWorldCoord)
                            if (SCNVector3.distanceFrom(vector: middlepoint, toVector: self.packageToFind.position) < 0.4) {
                            //if (abs(lengthR - lengthL) < 0.05) && (abs(lengthT - lengthB) < 0.05) {
                                print(topVector.dotProduct(leftVector))
                                if (abs(topVector.dotProduct(leftVector)) < 0.01 && abs(leftVector.dotProduct(bottomVector)) < 0.01 && abs(bottomVector.dotProduct(rightVector)) < 0.01) {
                                let length = (lengthR + lengthL) / 2
                                let width = (lengthT + lengthB) / 2
                                //print("Measurements ok")
                                let package = self.packageToFind
                                //Compare measured data to real package data
                                if self.compareRectToPackage(packageL: package.length, packageW: package.width, packageH: package.height, rectL: length, rectW: width) {
                                    let ciImage = CIImage(cvPixelBuffer: currentFrame.capturedImage)
                                    let deskewedImage = ciImage.perspectiveCorrected(topLeft: self.adjustToImageSize(point: prediction.topLeft, extent: ciImage.extent), topRight: self.adjustToImageSize(point: prediction.topRight, extent: ciImage.extent), bottomLeft: self.adjustToImageSize(point: prediction.bottomLeft, extent: ciImage.extent), bottomRight: self.adjustToImageSize(point: prediction.bottomRight, extent: ciImage.extent))

                                
                                    DispatchQueue.global(qos: .default).async {
                                        let colors = UIImage(ciImage: deskewedImage).getColors()
                                        print(colors)
                                        //Compare colors
                                        //if (package.front.getPrimary() == colors[0]) {
                                            var packageColors = Set(package.front.getSecondaryColors())
                                            packageColors.insert(package.front.getPrimary())
                                            if (packageColors == Set(colors)) {
                                                print("Package \(package.number) found! Front Side.")
                                                let distance = SCNVector3.distanceFrom(vector: self.currentCameraPosition, toVector: self.packageToFind.getPosition())
                                                let bubbleNode = self.createNewBubbleParentNode("HitTestLocation")
                                                bubbleNode.position = middlepoint
                                                self.sceneView.scene.rootNode.addChildNode(bubbleNode)
                                                self.rectangleDetectionActive = false
                                                DispatchQueue.main.async {
                                                    //self.previewImage.image = UIImage(ciImage: deskewedImage)
                                                    // to play sound
                                                    AudioServicesPlaySystemSound (self.systemSoundID)
                                                    let time = DispatchTime.now()
                                                    let nanoTime = time.uptimeNanoseconds - self.startTime.uptimeNanoseconds // <<<<< Difference in nano seconds (UInt64)
                                                    let timeInterval = Double(nanoTime) / 1_000_000_000
                                                    self.infoTextView.text.append("Found Package Number \(self.packageToFind.number) after: \(timeInterval) with distance \(distance )\n")
                                                    self.statusTextView.text = "Package \(self.packageToFind.number) found! Rectangle."
                                                }
                                                return
                                            }
                                        
                                        //if (package.top.getPrimary() == colors[0]) {
                                            var packageColorsTop = Set(package.top.getSecondaryColors())
                                            packageColorsTop.insert(package.top.getPrimary())
                                            if (packageColorsTop == Set(colors)) {
                                                print("Package \(package.number) found! Top Side.")
                                                let distance = SCNVector3.distanceFrom(vector: self.currentCameraPosition, toVector: self.packageToFind.getPosition())
                                                let bubbleNode = self.createNewBubbleParentNode("HitTestLocation")
                                                bubbleNode.position = middlepoint
                                                self.sceneView.scene.rootNode.addChildNode(bubbleNode)
                                                self.rectangleDetectionActive = false
                                                DispatchQueue.main.async {
                                                    
                                                    // Create a plane to visualize the initial position of the detected image.
                                                   
                                                    let plane = SCNPlane(width: CGFloat(lengthL),
                                                                             height: CGFloat(lengthT))
                                                    let planeNode = SCNNode(geometry: plane)
                                                    let orientation = (topRightWorldCoord - bottomRightWorldCoord).angleBetweenVectors(SCNVector3(x: 0, y: 0, z: -1))
                                                    
                                                    planeNode.eulerAngles.y = -orientation - (.pi/2)
                                                    planeNode.opacity = 0.25
                                                    
                                                    /*
                                                     `SCNPlane` is vertically oriented in its local coordinate space, but
                                                     `ARImageAnchor` assumes the image is horizontal in its local space, so
                                                     rotate the plane to match.
                                                     */
                                                    planeNode.eulerAngles.x = -.pi / 2
                                                    
                                                    /*
                                                     Image anchors are not tracked after initial detection, so create an
                                                     animation that limits the duration for which the plane visualization appears.
                                                     */
                                                    planeNode.position = middlepoint
                                                    planeNode.runAction(self.imageHighlightAction)
                                                    
                                                    // Add the plane visualization to the scene.
                                                    self.sceneView.scene.rootNode.addChildNode(planeNode)
                                                    
                                                    //self.previewImage.image = UIImage(ciImage: deskewedImage)
                                                    // to play sound
                                                    AudioServicesPlaySystemSound (self.systemSoundID)
                                                    let time = DispatchTime.now()
                                                    let nanoTime = time.uptimeNanoseconds - self.startTime.uptimeNanoseconds // <<<<< Difference in nano seconds (UInt64)
                                                    let timeInterval = Double(nanoTime) / 1_000_000_000
                                                    self.infoTextView.text.append("Number: \(self.packageToFind.number), time: \(timeInterval), distance \(distance)\n")
                                                    self.statusTextView.text = "Package \(self.packageToFind.number) found! Rectangle."
                                                    self.boundingBoxViews[i].hide()

                                                }
                                                return
                                            }
                                        
                                    }
                                }
                                
                                
                                }
                                
                            } else {
                                //print(SCNVector3.distanceFrom(vector: centerWorldCoord, toVector: self.packageToFind.position))
                            }
                            
                            self.boundingBoxViews[i].drawPolygon(points: convertedPoints, color: UIColor.green)
                        } else {
                            self.boundingBoxViews[i].hide()
                    }
                        
                    }
                }
            })
            
            // Don't limit resulting number of observations
            request.maximumObservations = 0
            let aspectRatio = min(self.packageToFind.width, self.packageToFind.length) / max(self.packageToFind.width, self.packageToFind.length)
            request.maximumAspectRatio = aspectRatio + 0.02
            request.minimumAspectRatio = aspectRatio - 0.02
            request.minimumConfidence = 0.7
            
            // Perform request
            let handler = VNImageRequestHandler(cvPixelBuffer: currentFrame.capturedImage, options: [:])
            try? handler.perform([request])
        }
    }
    
    
    func adjustToImageSize(point: CGPoint, extent: CGRect) -> CGPoint {
        var normalized = CGPoint()
        normalized.x = point.x * extent.width
        normalized.y = point.y * extent.height
        return normalized
    }
    
    func compareRects(rect1l: Float, rect1w: Float, rect2l: Float, rect2w: Float) -> Bool {
        return ((abs(rect1l - rect2l) < 0.025) && (abs(rect1w - rect2w) < 0.025)) || ((abs(rect1l - rect2w) < 0.025) && (abs(rect1w - rect2l) < 0.025))
    }
    
    func compareRectToPackage(packageL: Float, packageW: Float, packageH: Float, rectL: Float, rectW: Float) -> Bool {
        return (compareRects(rect1l: packageL, rect1w: packageW, rect2l: rectL, rect2w: rectW) || compareRects(rect1l: packageL, rect1w: packageH, rect2l: rectL, rect2w: rectW) || compareRects(rect1l: packageW, rect1w: packageH, rect2l: rectL, rect2w: rectW))
    }
    
   
    //Creates a node with text in the AR Scene
    //Source: https://github.com/hanleyweng/CoreML-in-ARKit/blob/master/CoreML%20in%20ARKit/ViewController.swift
    func createNewBubbleParentNode(_ text : String) -> SCNNode {
        
        // TEXT BILLBOARD CONSTRAINT
        let billboardConstraint = SCNBillboardConstraint()
        billboardConstraint.freeAxes = SCNBillboardAxis.Y
        
        // BUBBLE-TEXT
        let bubble = SCNText(string: text, extrusionDepth: CGFloat(bubbleDepth))
        var font = UIFont(name: "Futura", size: 0.15)
        font = font?.withTraits(traits: .traitBold)
        bubble.font = font
        bubble.firstMaterial?.diffuse.contents = UIColor.orange
        bubble.firstMaterial?.specular.contents = UIColor.white
        bubble.firstMaterial?.isDoubleSided = true
        // bubble.flatness // setting this too low can cause crashes.
        bubble.chamferRadius = CGFloat(bubbleDepth)
        
        // BUBBLE NODE
        let (minBound, maxBound) = bubble.boundingBox
        let bubbleNode = SCNNode(geometry: bubble)
        // Centre Node - to Centre-Bottom point
        bubbleNode.pivot = SCNMatrix4MakeTranslation( (maxBound.x - minBound.x)/2, minBound.y, bubbleDepth/2)
        // Reduce default text size
        bubbleNode.scale = SCNVector3Make(0.2, 0.2, 0.2)
        
        // CENTRE POINT NODE
        let sphere = SCNSphere(radius: 0.005)
        sphere.firstMaterial?.diffuse.contents = UIColor.cyan
        let sphereNode = SCNNode(geometry: sphere)
        
        // BUBBLE PARENT NODE
        let bubbleNodeParent = SCNNode()
        bubbleNodeParent.addChildNode(bubbleNode)
        bubbleNodeParent.addChildNode(sphereNode)
        bubbleNodeParent.constraints = [billboardConstraint]
        
        return bubbleNodeParent
    }
    
    //Highlights detected Marker
    var imageHighlightAction: SCNAction {
        return .sequence([
            .wait(duration: 0.25),
            .fadeOpacity(to: 0.85, duration: 0.25),
            .fadeOpacity(to: 0.15, duration: 0.25),
            .fadeOpacity(to: 0.85, duration: 0.25),
            .fadeOut(duration: 0.5),
            .removeFromParentNode()
            ])
    }
    
    //Convert Detected Rectangle Coordinates into Screen coordinates
    func convertFromCamera(_ point: CGPoint) -> CGPoint {
        let orientation = UIApplication.shared.statusBarOrientation
        
        //let width = sceneView.frame.width
        let height = sceneView.bounds.height
        let width = sceneView.bounds.height * 3 / 4
        let offsetX = (self.sceneView.bounds.width - width) / 2
        
        switch orientation {
        case .portrait, .unknown:
            return CGPoint(x: point.y * width + offsetX, y: point.x * height)
        case .landscapeLeft:
            return CGPoint(x: (1 - point.x) * width, y: point.y * height)
        case .landscapeRight:
            return CGPoint(x: point.x * width, y: (1 - point.y) * height)
        case .portraitUpsideDown:
            return CGPoint(x: (1 - point.y) * width, y: (1 - point.x) * height)
        }
    }
    
}



extension SCNVector3 {
    static func distanceFrom(vector vector1: SCNVector3, toVector vector2: SCNVector3) -> Float {
        let x0 = vector1.x
        let x1 = vector2.x
        let y0 = vector1.y
        let y1 = vector2.y
        let z0 = vector1.z
        let z1 = vector2.z
        
        return sqrtf(powf(x1-x0, 2) + powf(y1-y0, 2) + powf(z1-z0, 2))
    }
    /*
     SOURCE: https://gist.github.com/jeremyconkin/a3909b2d3276d1b6fbff02cefecd561a
 */
    
    /// Calculate the magnitude of this vector
    var magnitude:SCNFloat {
        get {
            return sqrt(dotProduct(self))
        }
    }
    
    /**
     Add two vectors
     
     - parameter left: Addend 1
     - parameter right: Addend 2
     */
    static func +(left:SCNVector3, right:SCNVector3) -> SCNVector3 {
        
        return SCNVector3(left.x + right.x, left.y + right.y, left.z + right.z)
    }
    
    /**
     Subtract two vectors
     
     - parameter left: Minuend
     - parameter right: Subtrahend
     */
    static func -(left:SCNVector3, right:SCNVector3) -> SCNVector3 {
        
        return left + (right * -1.0)
    }
    
    /**
     Add one vector to another
     
     - parameter left: Vector to change
     - parameter right: Vector to add
     */
    static func +=( left: inout SCNVector3, right:SCNVector3) {
        
        left = SCNVector3(left.x + right.x, left.y + right.y, left.z + right.z)
    }
    
    /**
     Subtract one vector to another
     
     - parameter left: Vector to change
     - parameter right: Vector to subtract
     */
    static func -=( left: inout SCNVector3, right:SCNVector3) {
        
        left = SCNVector3(left.x - right.x, left.y - right.y, left.z - right.z)
    }
    
    /**
     Multiply a vector times a constant
     
     - parameter vector: Vector to modify
     - parameter constant: Multiplier
     */
    static func *(vector:SCNVector3, multiplier:SCNFloat) -> SCNVector3 {
        
        return SCNVector3(vector.x * multiplier, vector.y * multiplier, vector.z * multiplier)
    }
    
    /**
     Multiply a vector times a constant and update the vector inline
     
     - parameter vector: Vector to modify
     - parameter constant: Multiplier
     */
    static func *=( vector: inout SCNVector3, multiplier:SCNFloat) {
        
        vector = vector * multiplier
    }
        
    /**
     Calculate the dot product of two vectors
     
     - parameter vectorB: Other vector in the calculation
     */
    func dotProduct(_ vectorB:SCNVector3) -> SCNFloat {
        
        return (self.x * vectorB.x) + (self.y * vectorB.y) + (self.z * vectorB.z)
    }
    
    func angleBetweenVectors(_ vectorB:SCNVector3) -> SCNFloat {
        
        //cos(angle) = (A.B)/(|A||B|)
        let cosineAngle = (dotProduct(vectorB) / (magnitude * vectorB.magnitude))
        return SCNFloat(acos(cosineAngle))
    }
    
    
    
    
    

}

extension UIFont {
    // Based on: https://stackoverflow.com/questions/4713236/how-do-i-set-bold-and-italic-on-uilabel-of-iphone-ipad
    func withTraits(traits:UIFontDescriptor.SymbolicTraits...) -> UIFont {
        let descriptor = self.fontDescriptor.withSymbolicTraits(UIFontDescriptor.SymbolicTraits(traits))
        return UIFont(descriptor: descriptor!, size: 0)
    }
}

extension float4x4 {
    var translation: float3 {
        let translation = self.columns.3
        return float3(translation.x, translation.y, translation.z)
    }
}

extension CGPoint {
    func midBetween(_ other: CGPoint) -> CGPoint {
        return CGPoint(x: (self.x + other.x) / 2.0,
                       y: (self.y + other.y) / 2.0)
    }
}
