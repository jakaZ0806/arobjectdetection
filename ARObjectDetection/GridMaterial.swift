//
//  GridMaterial.swift
//  ARObjectDetection
//
//  Created by ITC VC MBP on 19.02.19.
//  Copyright © 2019 MachineThink. All rights reserved.
//

import Foundation
import SceneKit
import ARKit
class GridMaterial: SCNMaterial {
    
    override init() {
        super.init()
        // 1
        let image = UIImage(named: "Grid")
        
        // 2
        diffuse.contents = image
        diffuse.wrapS = .repeat
        diffuse.wrapT = .repeat
    }
    
    func updateWith(anchor: ARPlaneAnchor) {
        // 1
        let mmPerMeter: Float = 1000
        let mmOfImage: Float = 65
        let repeatAmount: Float = mmPerMeter / mmOfImage
        
        // 2
        diffuse.contentsTransform = SCNMatrix4MakeScale(anchor.extent.x * repeatAmount, anchor.extent.z * repeatAmount, 1)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
