//
//  Plane.swift
//  ARObjectDetection
//
//  Created by ITC VC MBP on 19.02.19.
//  Copyright © 2019 MachineThink. All rights reserved.
//

import Foundation
import ARKit

class Plane: SCNNode {
    
    let plane: SCNPlane
    
    init(anchor: ARPlaneAnchor) {
        // 2
        plane = SCNPlane(width: CGFloat(anchor.extent.x), height: CGFloat(anchor.extent.z))
        
        super.init()
        
        // 3
        plane.cornerRadius = 0.008
        // 1
        plane.materials = [GridMaterial()]
        // ...
        // 2 delete this...
        
        
        // 4
        let planeNode = SCNNode(geometry: plane)
        planeNode.position = SCNVector3Make(anchor.center.x, 0, anchor.center.z)
        planeNode.opacity = 1
        
        // 5
        planeNode.eulerAngles.x = -.pi / 2
        
        
        // 7
        addChildNode(planeNode)
    }
    
    func updateWith(anchor: ARPlaneAnchor) {
        // 1
        plane.width = CGFloat(anchor.extent.x)
        plane.height = CGFloat(anchor.extent.z)
        // 2
        position = SCNVector3Make(anchor.center.x, 0, anchor.center.z)
        if let grid = plane.materials.first as? GridMaterial {
            grid.updateWith(anchor: anchor)
        }
    }
    
    // 8
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
}
}
