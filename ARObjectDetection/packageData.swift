//
//  packageData.swift
//  ARObjectDetection
//
//  Created by ITC VC MBP on 05.03.19.
//  Copyright © 2019 MachineThink. All rights reserved.
//

import Foundation
import UIKit
import ARKit

class packageData {
    var number: Int
    var length: Float
    var width: Float
    var height: Float
    var position: SCNVector3
    var barcodeValue: String
    
    var top: packageColors
    //var bottom: packageColors
    //var left: packageColors
    //var right: packageColors
    var front: packageColors
    //var back: packageColors
    
    init(number: Int, length: Float, width: Float, height: Float, top: packageColors, front: packageColors, position: SCNVector3, barcode: String) {
        self.number = number
        self.length = length
        self.width = width
        self.height = height
        self.top = top
        self.front = front
        self.position = position
        self.barcodeValue = barcode
        
    }
    func getPosition() -> SCNVector3 {
        return self.position
    }
}

class packageColors {
    var primary: String
    var others: [String]
    
    init(primary: String, others: [String]) {
        self.others = others
        self.primary = primary
    }
    
    func getSecondaryColors() -> [String] {
        return self.others
    }
    
    func getPrimary() -> String {
        return self.primary
    }
    
}
